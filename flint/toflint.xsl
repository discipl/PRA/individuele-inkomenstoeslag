<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8" indent="no" media-type="application/json"/>
<!-- experimental xsl script to transform wetten.nl xml to flint -->
<xsl:template match="/">
{
  "source" : {
						"citation": "<xsl:value-of select='toestand/wetgeving/citeertitel'/>",
						"juriconnect": "jci1.3:c:<xsl:value-of select='toestand/@*[name()="bwb-id"]'/>",
						"explanation" : ""
	},
	acts : [],
	facts : [<xsl:apply-templates select="node()"/>
  ],
  duties : []
}
</xsl:template>

<xsl:template match="artikel">
	    {
	    	fact : "[A<xsl:value-of select='kop/nr'/>]",
	    	function : "",
	    	sources : {
	    		"citation" : "<xsl:value-of select='@*[name()="bwb-ng-variabel-deel"]'/>",
	    		"juriconnect" : "<xsl:value-of select="meta-data/jcis/jci/@verwijzing"/>",
	    		"text" : "<xsl:value-of select="al"/>"
	    	},
	    	explanation : ""
	    },<xsl:apply-templates/>
</xsl:template>

<xsl:template match="lid">
	    {
	    	fact : "[A<xsl:value-of select='ancestor::artikel/kop/nr'/>L<xsl:value-of select='lidnr'/>]",
	    	function : "",
	    	sources : {
	    		"citation" : "<xsl:value-of select='@*[name()="bwb-ng-variabel-deel"]'/>",
	    		"juriconnect" : "<xsl:value-of select="meta-data/jcis/jci/@verwijzing"/>",
	    		"text" : "<xsl:value-of select="al"/>"
	    	},
	    	explanation : ""
	    },<xsl:apply-templates select="lijst"/>
</xsl:template>

<xsl:template match="li">
	    {
	    	fact : "[A<xsl:value-of select='ancestor::artikel/kop/nr'/>L<xsl:value-of select='ancestor::lid/lidnr'/>-<xsl:value-of select="substring-before(li.nr,'.')"/>]",
	    	function : "",
	    	sources : {
	    		"citation" : "<xsl:value-of select='@*[name()="bwb-ng-variabel-deel"]'/>",
	    		"juriconnect" : "<xsl:value-of select="meta-data/jcis/jci/@verwijzing"/>",
	    		"text" : "<xsl:value-of select="al"/>"
	    	},
	    	explanation : ""
	    },<xsl:apply-templates select="lijst"/>
</xsl:template>

<xsl:template match="lijst"><xsl:apply-templates select="li"/></xsl:template>

<xsl:template match="node()"><xsl:apply-templates/></xsl:template>

</xsl:stylesheet>
