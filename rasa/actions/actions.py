from typing import Dict, Text, Any, List, Union
from datetime import datetime

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction


class ValidateIndividueelInkomstenLoketForm(FormValidationAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "validate_iit_form"

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer."""
        try:
            int(string)
            return True
        except ValueError:
            return False

    @staticmethod
    def is_float(string: Text) -> bool:
        """Check if a string is a float."""

        try:
            float(string)
            return True
        except ValueError:
            return False

    def validate_bruto_inkomen_slot(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate bruto_inkomen_slot value."""

        if tracker.get_intent_of_latest_message() == 'vraag_waarom':
            dispatcher.utter_message(text = "Omdat uw inkomen niet hoger mag zijn dan X euro")
            return {"bruto_inkomen_slot": None}

        if self.is_float(value) and int(value) > 0:
            return {"bruto_inkomen_slot": value}
        else:
            dispatcher.utter_message(response="utter_wrong_bruto_inkomen_slot")
            # validation failed, set slot to None
            return {"bruto_inkomen_slot": None}

    def validate_aantal_kinderen_slot(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate aantal_kinderen_slot value."""

        if self.is_int(value) and int(value) > 0:
            return {"aantal_kinderen_slot": value}
        else:
            dispatcher.utter_message(response="utter_wrong_aantal_kinderen_slot")
            # validation failed, set slot to None
            return {"aantal_kinderen_slot": None}

    def validate_isgehuwdsamenwonend_slot(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate isgehuwdsamenwonend_slot value."""

        if isinstance(value, str):
            if "ja" in value:
                # convert "out..." to True
                return {"isgehuwdsamenwonend_slot": True}
            elif "nee" in value:
                # convert "in..." to False
                return {"isgehuwdsamenwonend_slot": False, "aantal_kinderen_slot": "0" }
        
        dispatcher.utter_message(response="utter_wrong_isgehuwdsamenwonend_slot")
        # validation failed, set slot to None
        return {"isgehuwdsamenwonend_slot": None}

class ValidateIndividueelInkomstenLoketForm1(FormValidationAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "validate_iit_form1"

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer."""
        try:
            int(string)
            return True
        except ValueError:
            return False

    @staticmethod
    def is_float(string: Text) -> bool:
        """Check if a string is a float."""

        try:
            float(string)
            return True
        except ValueError:
            return False

    def validate_alles_voor_formulier1_slot(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate all values."""

        values = value.split(" ")

        brutoInkomen = None
        if self.is_float(values[0]) and int(values[0]) >= 0:
            brutoInkomen = values[0]
        else:
            dispatcher.utter_message(response="utter_wrong_bruto_inkomen_slot")
            # validation failed, set slot to None
            return {"alles_voor_formulier1_slot": None}

        isGehuwdBool = None
        if isinstance(values[1], str):
            if "ja" in values[1]:
                isGehuwdBool = True
            elif "nee" in values[1]:
                isGehuwdBool = False
            else:
                dispatcher.utter_message(response="utter_wrong_isgehuwdsamenwonend_slot")
                return {"alles_voor_formulier1_slot": None}
        else:
            dispatcher.utter_message(response="utter_wrong_isgehuwdsamenwonend_slot")
            return {"alles_voor_formulier1_slot": None}
        
        aantalKinderen = 0
        if self.is_float(values[2]) and int(values[2]) >= 0:
            aantalKinderen = values[2]
        else:
            dispatcher.utter_message(response="utter_wrong_aantal_kinderen_slot")
            return {"alles_voor_formulier1_slot": None}

        # validation failed, set slot to None
        return {"bruto_inkomen_slot": brutoInkomen, "isgehuwdsamenwonend_slot": isGehuwdBool, "aantal_kinderen_slot": aantalKinderen }

class ValidateIndividueelInkomstenLoketForm2(FormValidationAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "validate_iit_form2"

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer."""
        try:
            int(string)
            return True
        except ValueError:
            return False

    @staticmethod
    def is_float(string: Text) -> bool:
        """Check if a string is a float."""

        try:
            float(string)
            return True
        except ValueError:
            return False

    def validate_alles_voor_formulier2_slot(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate all values."""

        values = value.split(" ")

        brutoInkomen = None
        if self.is_float(values[0]) and int(values[0]) >= 0:
            brutoInkomen = values[0]
        else:
            dispatcher.utter_message(response="utter_wrong_bruto_inkomen_slot")
            # validation failed, set slot to None
            return {"alles_voor_formulier2_slot": None}

        isGehuwdBool = None
        if isinstance(values[1], str):
            if "ja" in values[1]:
                isGehuwdBool = True
            elif "nee" in values[1]:
                isGehuwdBool = False
            else:
                dispatcher.utter_message(response="utter_wrong_isgehuwdsamenwonend_slot")
                return {"alles_voor_formulier2_slot": None}
        else:
            dispatcher.utter_message(response="utter_wrong_isgehuwdsamenwonend_slot")
            return {"alles_voor_formulier2_slot": None}
        
        aantalKinderen = 0
        if self.is_float(values[2]) and int(values[2]) >= 0:
            aantalKinderen = values[2]
        else:
            dispatcher.utter_message(response="utter_wrong_aantal_kinderen_slot")
            return {"alles_voor_formulier2_slot": None}

        # validation failed, set slot to None
        return {"bruto_inkomen_slot": brutoInkomen, "isgehuwdsamenwonend_slot": isGehuwdBool, "aantal_kinderen_slot": aantalKinderen }

class ActionTellTime(Action):
    def name(self) -> Text:
        return "action_tell_time"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        timeNow = datetime.now()
        dt_string = timeNow.strftime("%d/%m/%Y %H:%M:%S")
        dispatcher.utter_message(text = "Het is: " + dt_string)

        return []